<h1>Hello</h1>

<h2>DB Models</h2>

<h3>User</h3>
<li>Email    string
<li>Login    string
<li>Password string
<li>Name     string
<li>Surname  string
<li>Token    string
<li>Role     uint

<h3>Task</h3>
<li>Title    string </li>
<li>Text     string </li>
<li>Category uint </li>
<li>Author   string </li>
<li>Status   string </li>
<li>Deadline string </li>
<li>UserId   uint </li>

<h3>Category</h3>
<li>СategType    string </li>

<h3>Roles</h3>
<li>Role    string </li>


<h2>Router</h2>
<h3>User</h3>
<ul>
    <li>POST on <b>/api/user/new</b> is handled by CreateUser handler. Requires email, login, name, surname and password data in request Body 
    </li>
    <li>POST on <b>/api/user/login</b> is handled by Authenticate handler. Requires email and password data in request Body and JWT token in Header </li>
</ul>
<h3>Task</h3>
<ul>
    <li>POST on /api/task requires Title, Text, Category, Author, Status and Deadline data in request Body and JWT token in Header</li>
    <li>GET on /api/task returns all tasks of user</li>
    <li>PUT on /api/task/{taskId} updates task by ID. Requires Title, Text, Category, Author, Status or Deadline data in request Body and JWT token in Header</li>
    <li>DELETE on /api/task/{taskId} deletes task by ID</li>
    <li>GET on /api/task/{categoryId} returns all tasks with provided category ID </li>
</ul>
<h3>Category</h3>
<ul>
    <li>GET on /api/category/ returns all the categories from the database</li>
    <li>POST on /api/category/ requires СategType data in request body. Only Admin can create a category</li>
    <li>DELETE on /api/category/{categoryId} deletes a category from table. Only Admin can delete a category</li>
    <li>GET on /api/category/{categoryId} returns the category by ID</li>
</ul>
<h3>Role</h3>
<ul>
    <li>GET on /api/role/ - shows all users with admin privileges</li>
    <li>PUT on /api/role/give-admin/{userId} gives admin privileges to user with provided ID</li>
    <li>PUT on /api/role/delete-admin/{userId} takes admin privileges from user with provided ID</li>
</ul>
