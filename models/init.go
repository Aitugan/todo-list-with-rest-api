package models

import (
	"fmt"

	"github.com/go-redis/redis"
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/postgres"
	"github.com/lib/pq"
)

var (
	db     *gorm.DB
	client *redis.Client
)

func init() {
	dbUri, err := pq.ParseURL("postgres://mdqtemsu:gvtDbsKCdF6eUiD6Ap0nahB7pJSzZ0wA@john.db.elephantsql.com:5432/mdqtemsu")
	if err != nil {
		fmt.Print(err)
	}
	client = redis.NewClient(&redis.Options{
		Addr:     "localhost:6379",
		Password: "",
		DB:       0,
	})
	conn, err := gorm.Open("postgres", dbUri)
	if err != nil {
		fmt.Print(err)
	}

	db = conn
	db.Debug().AutoMigrate(&User{}, &Task{}, &Roles{}, &Category{})
}

func GetDB() *gorm.DB {
	return db
}

func GetRedis() *redis.Client {
	return client
}
