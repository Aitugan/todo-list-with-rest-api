package models

import (
	"github.com/jinzhu/gorm"
)

// A categories model
type Roles struct {
	gorm.Model
	Role string `json:"role"`
}

func NewRole() *Roles {
	return &Roles{}
}
