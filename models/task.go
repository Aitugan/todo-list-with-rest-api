package models

import (
	"github.com/jinzhu/gorm"
)

type Task struct {
	gorm.Model
	Title    string `json:"title"`
	Text     string `json:"text"`
	Category uint   `json:"category"`
	Author   string `json:"author"`
	Status   string `json:"status"`
	Deadline string `json:"deadline"`
	UserId   uint   `json:"user_id"`
}

func NewTask() *Task {
	return &Task{}
}
