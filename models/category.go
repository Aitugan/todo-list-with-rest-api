package models

import (
	"github.com/jinzhu/gorm"
	// u "gitlab.com/Aitugan/task2/utils"
)

type Category struct {
	gorm.Model
	СategType string `json:"categType"`
}

func NewCategory() *Category {
	return &Category{}
}
