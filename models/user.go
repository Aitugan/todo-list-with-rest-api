package models

import (
	"github.com/dgrijalva/jwt-go"
	"github.com/jinzhu/gorm"
)

// Struct for JWT claims
type Token struct {
	UserId uint
	jwt.StandardClaims
}

// A user model
type User struct {
	gorm.Model
	Email    string `json:"email"`
	Login    string `json:"login"`
	Password string `json:"password"`
	Name     string `json:"name"`
	Surname  string `json:"surname"`
	Token    string `json:"token";sql:"-"`
	Role     uint   `json:"role";gorm:"ForeignKey:Uid"`
}

func NewUser() *User {
	return &User{}
}
