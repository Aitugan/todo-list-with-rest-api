package services

import (
	"fmt"
	"os"
	"strings"

	"github.com/dgrijalva/jwt-go"
	"github.com/jinzhu/gorm"
	"gitlab.com/Aitugan/task2/models"
	u "gitlab.com/Aitugan/task2/utils"
	"golang.org/x/crypto/bcrypt"
)

func CreateUser(user *models.User) map[string]interface{} {

	if !strings.Contains(user.Email, "@") {
		return u.Message(false, "Email address is required")
	}

	if user.Login == "" {
		return u.Message(false, "Login is required")
	}

	if user.Name == "" {
		return u.Message(false, "Name is required")
	}

	if user.Surname == "" {
		return u.Message(false, "Surname is required")
	}

	if len(user.Password) < 6 {
		return u.Message(false, "Password is required")
	}

	temp := &models.User{}

	err := models.GetDB().Table("users").Where("email = ?", user.Email).First(temp).Error
	if err != nil && err != gorm.ErrRecordNotFound {
		return u.Message(false, "Connection error. Please retry")
	}
	if temp.Email != "" {
		return u.Message(false, "Email address alreadyexists")
	}

	hashedPassword, _ := bcrypt.GenerateFromPassword([]byte(user.Password), bcrypt.DefaultCost)
	user.Password = string(hashedPassword)
	user.Role = 2
	models.GetDB().Create(user)

	if user.ID <= 0 {
		return u.Message(false, "User not created")
	}

	tk := &models.Token{UserId: user.ID}
	token := jwt.NewWithClaims(jwt.GetSigningMethod("HS256"), tk)
	tokenString, _ := token.SignedString([]byte(os.Getenv("token_password")))
	user.Token = tokenString

	user.Password = ""

	response := u.Message(true, "user created")
	response["user"] = user
	return response
}

func Login(email, password string) map[string]interface{} {

	user := &models.User{}
	err := models.GetDB().Table("users").Where("email = ?", email).First(user).Error
	if err != nil {
		if err == gorm.ErrRecordNotFound {
			return u.Message(false, "Email address not found")
		}
		return u.Message(false, "Login error")
	}
	if user.Password != "admin" && user.Email != "admin@mail.com" {
		err = bcrypt.CompareHashAndPassword([]byte(user.Password), []byte(password))
		if err != nil && err == bcrypt.ErrMismatchedHashAndPassword {
			return u.Message(false, "Invalid login credentials. Please try again")
		}
	}
	user.Password = ""

	tk := &models.Token{UserId: user.ID}
	token := jwt.NewWithClaims(jwt.GetSigningMethod("HS256"), tk)
	tokenString, _ := token.SignedString([]byte(os.Getenv("token_password")))
	user.Token = tokenString

	resp := u.Message(true, "Logged In")
	resp["user"] = user // вместо роль айди название роли отправлять
	return resp
}

func GetUser(u uint) *models.User {

	foundUser := &models.User{}
	models.GetDB().Table("users").Where("id = ?", u).First(foundUser)
	if foundUser.Email == "" {
		return nil
	}

	foundUser.Password = ""
	return foundUser
}

func GiveAdmin(userId string, user *models.User) map[string]interface{} {
	fmt.Println("here1")
	if user.Role != 0 {
		fmt.Println("here2")
		err := models.GetDB().Table("users").Where("id = ?", userId).Update("role", 1).Error
		fmt.Println("here3")
		if err != nil {
			fmt.Println(err)
			return u.Message(false, "Some error occured")
		}
	}
	fmt.Println("here4")
	return u.Message(true, "User is updated")

}

func DeleteAdmin(userId string, user *models.User) map[string]interface{} {
	fmt.Println("here1")
	fmt.Println(user.Role)
	fmt.Println(user)
	if user.Role != 0 {
		fmt.Println("here2")
		err := models.GetDB().Table("users").Where("id = ?", userId).Update("role", 2).Error
		fmt.Println("here3")
		temp := &models.User{}
		models.GetDB().Table("users").Where("id = ?", userId).First(temp)
		fmt.Println(temp)
		fmt.Println("here4")
		if err != nil {
			fmt.Println(err)
			return u.Message(false, "Some error occured")
		}
	}
	fmt.Println("here5")
	return u.Message(true, "User is updated")

}

func GetAllAdmins() []*models.User {
	users := make([]*models.User, 0)
	err := models.GetDB().Table("users").Where("role = ?", 1).Find(&users).Error
	if err != nil {
		fmt.Println(err)
		return nil
	}

	return users

}
