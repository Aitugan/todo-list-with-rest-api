package services

import (
	"encoding/json"
	"fmt"
	"strconv"

	"gitlab.com/Aitugan/task2/models"
	u "gitlab.com/Aitugan/task2/utils"
)

func CreateTask(task *models.Task) map[string]interface{} {
	fmt.Println("HERE create task")
	if task.Title == "" {
		return u.Message(false, "Task Title should be on the payload")
	}

	if task.Text == "" {
		return u.Message(false, "Task Text should be on the payload")
	}

	if task.Category == 0 {
		return u.Message(false, "Task Category should be on the payload")
	}

	if task.Author == "" {
		return u.Message(false, "Author number should be on the payload")
	}

	if task.Status == "" {
		return u.Message(false, "Task Status should be on the payload")
	}

	if task.Deadline == "" {
		return u.Message(false, "Task Deadline should be on the payload")
	}

	if task.UserId <= 0 {
		return u.Message(false, "User is not recognized")
	}

	models.GetDB().Create(task)

	// err = client.Set("id1234", json, 0).Err()
	tasks := make([]*models.Task, 0)
	err := models.GetDB().Table("tasks").Order("created_at desc").Limit(10).Find(&tasks).Error
	if err != nil {
		fmt.Println(err)
		return nil
	}
	for _, task := range tasks {
		json, err := json.Marshal(task)
		if err != nil {
			fmt.Println(err)
		}
		models.GetRedis().Set(strconv.Itoa(int(task.UserId)), json, 0)
	}

	resp := u.Message(true, "success")
	resp["task"] = task
	return resp
}

func GetTask(id uint) *models.Task {

	task := &models.Task{}
	err := models.GetDB().Table("tasks").Where("user_id = ?", id).First(task).Error
	if err != nil {
		return nil
	}
	// categ := &Category{}
	// err := GetDB().Table("categories").Where("id = ?", task.Category).First(categ).Error
	// if err != nil {
	// 	return nil
	// }
	// task.Category = categ
	return task
}

func GetTasks(user uint) []*models.Task {

	tasks := make([]*models.Task, 0)
	err := models.GetDB().Table("tasks").Where("user_id = ?", user).Find(&tasks).Error
	if err != nil {
		fmt.Println(err)
		return nil
	}
	fmt.Println(tasks)
	return tasks
}

func GetTasksByCategory(user uint, category string) []*models.Task {
	tasks := make([]*models.Task, 0)
	err := models.GetDB().Table("tasks").Where("user_id = ?", user).Where("category = ?", category).Find(&tasks).Error
	if err != nil {
		fmt.Println(err)
		return nil
	}

	return tasks
}

func UpdateTask(user uint, taskId string, task *models.Task) map[string]interface{} {
	if task.Title != "" {
		err := models.GetDB().Table("tasks").Where("user_id = ?", user).Where("id = ?", taskId).Update("title", task.Title).Error
		if err != nil {
			fmt.Println(err)
			return u.Message(false, "Some error occured")
		}
	}

	if task.Text != "" {
		err := models.GetDB().Table("tasks").Where("user_id = ?", user).Where("id = ?", taskId).Update("text", task.Text).Error
		if err != nil {
			fmt.Println(err)
			return u.Message(false, "Some error occured")
		}
	}

	if task.Category != 0 {
		err := models.GetDB().Table("tasks").Where("user_id = ?", user).Where("id = ?", taskId).Update("category", task.Category).Error
		if err != nil {
			fmt.Println(err)
			return u.Message(false, "Some error occured")
		}
	}

	if task.Author != "" {
		err := models.GetDB().Table("tasks").Where("user_id = ?", user).Where("id = ?", taskId).Update("author", task.Author).Error
		if err != nil {
			fmt.Println(err)
			return u.Message(false, "Some error occured")
		}
	}

	if task.Status != "" {
		err := models.GetDB().Table("tasks").Where("user_id = ?", user).Where("id = ?", taskId).Update("status", task.Status).Error
		if err != nil {
			fmt.Println(err)
			return u.Message(false, "Some error occured")
		}
	}

	if task.Deadline != "" {
		err := models.GetDB().Table("tasks").Where("user_id = ?", user).Where("id = ?", taskId).Update("deadline", task.Deadline).Error
		if err != nil {
			fmt.Println(err)
			return u.Message(false, "Some error occured")
		}
	}

	if task.UserId <= 0 {
		return u.Message(false, "User is not recognized")
	}

	return u.Message(true, "Task is updated")
}

func DeleteTask(user uint, taskId string, task *models.Task) map[string]interface{} {
	err := models.GetDB().Table("tasks").Where("user_id = ?", user).Where("id = ?", taskId).Delete(task).Error

	if err != nil {
		fmt.Println(err)
		return u.Message(false, "User is not recognized or task doesn't exist")
	}
	return u.Message(true, "Task is deleted")
}
