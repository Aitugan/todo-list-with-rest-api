package services

import (
	"fmt"

	"gitlab.com/Aitugan/task2/models"
	u "gitlab.com/Aitugan/task2/utils"
)

func CreateCategory(category *models.Category) map[string]interface{} {
	if category.СategType == "" {
		return u.Message(false, "A category should be declared")
	}

	err := models.GetDB().Create(category)
	if err != nil {
		return nil
	}

	resp := u.Message(true, "success")
	resp["categories"] = category
	return resp
}

func GetCategory(id string) *models.Category {

	category := &models.Category{}
	err := models.GetDB().Table("categories").Where("id = ?", id).First(category).Error
	if err != nil {
		return nil
	}
	return category
}

func GetCategories() []*models.Category {

	categories := make([]*models.Category, 0)
	err := models.GetDB().Table("categories").Find(&categories).Error
	if err != nil {
		fmt.Println(err)
		return nil
	}

	return categories
}

func Delete(categoryId string, category *models.Category) map[string]interface{} {
	err := models.GetDB().Table("categories").Where("id = ?", categoryId).Delete(category).Error

	if err != nil {
		fmt.Println(err)
		return u.Message(false, "User is not recognized or task doesn't exist")
	}
	return u.Message(true, "Category is deleted")
}
