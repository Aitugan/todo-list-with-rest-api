package routes

import (
	"github.com/gorilla/mux"
	"gitlab.com/Aitugan/task2/app"
	"gitlab.com/Aitugan/task2/controllers"
)

func NewRoutes() *mux.Router {
	router := mux.NewRouter()
	base := router.PathPrefix("/api").Subrouter()

	user := base.PathPrefix("/user").Subrouter()
	user.HandleFunc("/new", controllers.CreateUser).Methods("POST")
	user.HandleFunc("/login", controllers.Authenticate).Methods("POST")

	task := base.PathPrefix("/task").Subrouter()
	task.HandleFunc("/", controllers.CreateTask).Methods("POST")
	task.HandleFunc("/", controllers.GetTasks).Methods("GET")
	task.HandleFunc("/{taskId}", controllers.UpdateTasks).Methods("PUT")
	task.HandleFunc("/{taskId}", controllers.DeleteTasks).Methods("DELETE")
	task.HandleFunc("/{categoryId}", controllers.GetTasksByCategory).Methods("GET")

	category := base.PathPrefix("/category").Subrouter()
	category.HandleFunc("/", controllers.GetCategories).Methods("GET")
	category.HandleFunc("/", controllers.CreateCategory).Methods("POST")
	category.HandleFunc("/{categoryId}", controllers.DeleteCategory).Methods("DELETE")
	category.HandleFunc("/{categoryId}", controllers.GetCategory).Methods("GET")

	role := base.PathPrefix("/role").Subrouter()
	role.HandleFunc("/give-admin/{userId}", controllers.GiveAdmin).Methods("PUT")
	role.HandleFunc("/delete-admin/{userId}", controllers.DeleteAdmin).Methods("PUT")
	role.HandleFunc("/", controllers.AllAdmins).Methods("GET")

	router.Use(app.JwtAuthentication)
	return router
}
