package main

import (
	"fmt"
	"log"
	"math/rand"
	"net/http"
	"os"
	"strconv"
	"strings"
	"time"

	"github.com/urfave/cli"
)

func main() {
	app := cli.NewApp()
	app.Name = "Filler"
	app.Usage = "DB filler CLI"
	app.Description = "For filling the database with user of task data"
	app.UsageText = "main <op> <amount> <token>"
	app.Version = "1.0.0"
	app.Authors = []cli.Author{
		cli.Author{
			Name: "Aitugan Mirash",
		},
	}

	app.Commands = []cli.Command{
		{ //Example. Adds 4 users
			//go run main.go adduser 4
			Name:    "adduser",
			Aliases: []string{"au"},
			Usage:   "Insert given",
			Action: func(c *cli.Context) error {
				count, err := strconv.Atoi(c.Args().Get(0))
				if err != nil {
					panic(err)
				}
				for i := 0; i < count; i++ {
					id := strconv.Itoa(rand.Intn(10000) + 1000)
					reader := strings.NewReader(`{
					"Email":"Email` + id + `@mail.com",
					"Login":"Login` + id + `",
					"Password":"Password` + id + `",
					"Name":"Name` + id + `",
					"Surname":"Surname` + id + `"}`)
					request, err := http.NewRequest("POST", "http://localhost:8000/api/user/new", reader)
					client := &http.Client{}
					resp, err := client.Do(request)
					if err != nil {
						panic(err)
					}
					fmt.Println(resp)
				}
				return nil
			},
		},
		{ //Example
			//go run main.go addtask 4 eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJVc2VySWQiOjR9.pg7tXSq9vcx8WAHjrnLcTKg8Z-x83nvY7SYYEye4ZxM
			Name:    "addtask",
			Aliases: []string{"at"},
			Usage:   "add two numbers",
			Action: func(c *cli.Context) error {
				count, err := strconv.Atoi(c.Args().Get(0))
				if err != nil {
					panic(err)
				}
				token := c.Args().Get(1)
				for i := 0; i < count; i++ {
					id := strconv.Itoa(rand.Intn(10000) + 1000)
					data := strings.NewReader(`{
					"Title":"Bot Title` + id + `",
					"Text":"Bot Text` + id + `",
					"Category":` + strconv.Itoa(rand.Intn(2)) + `,
					"Author":"Bot Author` + id + `",
					"Status":"In Progress",
					"Deadline":"` + time.Now().Format("2006.01.02") + `"}`)
					request, err := http.NewRequest("POST", "http://localhost:8000/api/task/", data)
					fmt.Println("Bearer " + token)
					request.Header.Set("Authorization", "Bearer "+token)
					client := &http.Client{}
					resp, err := client.Do(request)
					if err != nil {
						panic(err)
					}
					fmt.Println(resp)
				}
				return nil
			},
		},
	}

	err := app.Run(os.Args)
	if err != nil {
		log.Fatal(err)
	}

}
