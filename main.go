package main

import (
	"fmt"
	"net/http"
	"os"

	"gitlab.com/Aitugan/task2/routes"
)

func main() {
	router := routes.NewRoutes()
	port := os.Getenv("PORT")
	if port == "" {
		port = "8000"
	}
	fmt.Println(port)
	if err := http.ListenAndServe(":"+port, router); err != nil {
		fmt.Print(err)
	}
}
