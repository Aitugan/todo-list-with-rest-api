package controllers

import (
	"net/http"

	"github.com/gorilla/mux"
	"gitlab.com/Aitugan/task2/models"
	"gitlab.com/Aitugan/task2/services"
	u "gitlab.com/Aitugan/task2/utils"
)

func GiveAdmin(w http.ResponseWriter, r *http.Request) {
	userId := r.Context().Value("user").(uint)
	user := models.NewUser()
	role := models.NewRole()

	err := models.GetDB().Table("users").Where("id = ?", userId).First(user).Error
	if err != nil {
		u.Respond(w, u.Message(false, "Error while decoding request body"))
		return
	}
	err = models.GetDB().Table("roles").Where("id = ?", user.Role).First(role).Error
	if err != nil {
		u.Respond(w, u.Message(false, "Error while decoding request body"))
		return
	}

	params := mux.Vars(r)
	id := params["userId"]

	if role.Role == "admin" {
		resp := services.GiveAdmin(id, user)
		u.Respond(w, resp)
	} else {
		u.Respond(w, u.Message(false, "You need to be an admin!"))
		return
	}
}

func DeleteAdmin(w http.ResponseWriter, r *http.Request) {
	userId := r.Context().Value("user").(uint)
	user := models.NewUser()
	role := models.NewRole()

	err := models.GetDB().Table("users").Where("id = ?", userId).First(user).Error
	if err != nil {
		u.Respond(w, u.Message(false, "Error while decoding request body"))
		return
	}
	err = models.GetDB().Table("roles").Where("id = ?", user.Role).First(role).Error
	if err != nil {
		u.Respond(w, u.Message(false, "Error while decoding request body"))
		return
	}

	params := mux.Vars(r)
	id := params["userId"]

	if role.Role == "admin" {
		resp := services.DeleteAdmin(id, user)
		u.Respond(w, resp)
	} else {
		u.Respond(w, u.Message(false, "You need to be an admin!"))
		return
	}
}

func AllAdmins(w http.ResponseWriter, r *http.Request) {
	data := services.GetAllAdmins()
	resp := u.Message(true, "success")
	resp["data"] = data
	u.Respond(w, resp)

}
