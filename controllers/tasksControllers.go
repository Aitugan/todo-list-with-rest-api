package controllers

import (
	"encoding/json"
	"net/http"

	"github.com/gorilla/mux"
	"gitlab.com/Aitugan/task2/models"
	"gitlab.com/Aitugan/task2/services"
	u "gitlab.com/Aitugan/task2/utils"
)

func CreateTask(w http.ResponseWriter, r *http.Request) {
	// Take the id of the user that send the request
	user := r.Context().Value("user").(uint)
	task := models.NewTask()

	err := json.NewDecoder(r.Body).Decode(task)
	if err != nil {
		u.Respond(w, u.Message(false, "Error while decoding request body"))
		return
	}

	task.UserId = user
	resp := services.CreateTask(task)
	u.Respond(w, resp)
}

func GetTasks(w http.ResponseWriter, r *http.Request) {

	id := r.Context().Value("user").(uint)
	data := services.GetTasks(id)
	resp := u.Message(true, "success")
	resp["data"] = data
	u.Respond(w, resp)
}

func GetTasksByCategory(w http.ResponseWriter, r *http.Request) {

	id := r.Context().Value("user").(uint)
	params := mux.Vars(r)
	categoryId := params["categoryId"]
	data := services.GetTasksByCategory(id, categoryId)
	resp := u.Message(true, "success")
	resp["data"] = data
	u.Respond(w, resp)
}

func UpdateTasks(w http.ResponseWriter, r *http.Request) {
	user := r.Context().Value("user").(uint)
	task := models.NewTask()
	params := mux.Vars(r)
	taskId := params["taskId"]
	err := json.NewDecoder(r.Body).Decode(task)
	if err != nil {
		u.Respond(w, u.Message(false, "Error while decoding request body"))
		return
	}

	task.UserId = user
	resp := services.UpdateTask(user, taskId, task)
	u.Respond(w, resp)
}

func DeleteTasks(w http.ResponseWriter, r *http.Request) {
	id := r.Context().Value("user").(uint)
	task := models.NewTask()
	params := mux.Vars(r)
	taskId := params["taskId"]

	// err := json.NewDecoder(r.Body).Decode(task)
	// if err != nil {
	// 	u.Respond(w, u.Message(false, "Error while decoding request body"))
	// 	return
	// }

	task.UserId = id
	resp := services.DeleteTask(id, taskId, task)
	u.Respond(w, resp)
}
