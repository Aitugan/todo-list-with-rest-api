package controllers

import (
	"encoding/json"
	"net/http"

	"github.com/gorilla/mux"
	"gitlab.com/Aitugan/task2/models"
	"gitlab.com/Aitugan/task2/services"
	u "gitlab.com/Aitugan/task2/utils"
)

func CreateCategory(w http.ResponseWriter, r *http.Request) {
	// Take the id of the user that send the request
	userId := r.Context().Value("user").(uint)
	category := models.NewCategory()
	err := json.NewDecoder(r.Body).Decode(category)
	if err != nil {
		u.Respond(w, u.Message(false, "Invalid request"))
		return
	}
	user := models.NewUser()
	role := models.NewRole()

	err = models.GetDB().Table("users").Where("id = ?", userId).First(user).Error
	if err != nil {
		u.Respond(w, u.Message(false, "Error while decoding request body"))
		return
	}
	err = models.GetDB().Table("roles").Where("id = ?", user.Role).First(role).Error
	if err != nil {
		u.Respond(w, u.Message(false, "Error while decoding request body"))
		return
	}

	if err != nil {
		u.Respond(w, u.Message(false, "Error while decoding request body"))
		return
	}

	if role.Role == "admin" {
		resp := services.CreateCategory(category)
		u.Respond(w, resp)
	} else {
		u.Respond(w, u.Message(false, "You need to be an admin!"))
		return
	}
}

func GetCategories(w http.ResponseWriter, r *http.Request) {
	// id := r.Context().Value("user").(uint)
	data := services.GetCategories()
	resp := u.Message(true, "success")
	resp["data"] = data
	u.Respond(w, resp)
}

func GetCategory(w http.ResponseWriter, r *http.Request) {
	// id := r.Context().Value("user").(uint)
	params := mux.Vars(r)
	categoryId := params["categoryId"]
	data := services.GetCategory(categoryId)
	resp := u.Message(true, "success")
	resp["data"] = data
	u.Respond(w, resp)
}

func DeleteCategory(w http.ResponseWriter, r *http.Request) {
	userId := r.Context().Value("user").(uint)
	category := models.NewCategory()
	user := models.NewUser()
	role := models.NewRole()

	err := models.GetDB().Table("users").Where("id = ?", userId).First(user).Error
	if err != nil {
		u.Respond(w, u.Message(false, "Error while decoding request body"))
		return
	}
	err = models.GetDB().Table("roles").Where("id = ?", user.Role).First(role).Error
	if err != nil {
		u.Respond(w, u.Message(false, "Error while decoding request body"))
		return
	}
	params := mux.Vars(r)
	categoryId := params["categoryId"]

	if role.Role == "admin" {
		resp := services.Delete(categoryId, category)
		u.Respond(w, resp)
	} else {
		u.Respond(w, u.Message(false, "You need to be an admin!"))
		return
	}
}
