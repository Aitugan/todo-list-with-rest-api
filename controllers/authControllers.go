package controllers

import (
	"encoding/json"
	"net/http"

	"gitlab.com/Aitugan/task2/models"
	"gitlab.com/Aitugan/task2/services"
	u "gitlab.com/Aitugan/task2/utils"
)

func CreateUser(w http.ResponseWriter, r *http.Request) {

	user := models.NewUser()
	err := json.NewDecoder(r.Body).Decode(user)
	if err != nil {
		u.Respond(w, u.Message(false, "Invalid request"))
		return
	}
	resp := services.CreateUser(user)
	u.Respond(w, resp)
}

func Authenticate(w http.ResponseWriter, r *http.Request) {

	user := models.NewUser()
	err := json.NewDecoder(r.Body).Decode(user)
	if err != nil {
		u.Respond(w, u.Message(false, "Invalid request"))
		return
	}

	resp := services.Login(user.Email, user.Password)
	u.Respond(w, resp)
}
